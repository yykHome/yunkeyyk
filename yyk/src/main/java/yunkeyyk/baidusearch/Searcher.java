package yunkeyyk.baidusearch;

/**
 * Created by Administrator on 2017/6/1.
 */

import java.util.List;

public interface Searcher {
    public List<Webpage> search(String keyword);
    public List<Webpage> search(String keyword, int page);
}
