package yunkeyyk.baidusearch;

/**
 * Created by Administrator on 2017/6/1.
 */

import java.util.List;

public interface BaiduSearcher extends Searcher {

    /**
     * 新闻搜索
     * @param keyword
     * @return
     */
    public List<Webpage> searchNews(String keyword);
    /**
     * 新闻搜索（分页）
     * @param keyword
     * @param page
     * @return
     */
    public List<Webpage> searchNews(String keyword, int page);
    /**
     * 贴吧搜索
     * @param keyword
     * @return
     */
    public List<Webpage> searchTieba(String keyword);
    /**
     * 贴吧搜索（分页）
     * @param keyword
     * @param page
     * @return
     */
    public List<Webpage> searchTieba(String keyword, int page);
    /**
     * 知道搜索
     * @param keyword
     * @return
     */
    public List<Webpage> searchZhidao(String keyword);
    /**
     * 知道搜索（分页）
     * @param keyword
     * @param page
     * @return
     */
    public List<Webpage> searchZhidao(String keyword, int page);
    /**
     * 文库搜索
     * @param keyword
     * @return
     */
    public List<Webpage> searchWenku(String keyword);
    /**
     * 文库搜索（分页）
     * @param keyword
     * @param page
     * @return
     */
    public List<Webpage> searchWenku(String keyword, int page);
}

