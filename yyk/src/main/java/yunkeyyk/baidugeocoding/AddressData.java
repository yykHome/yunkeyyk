package yunkeyyk.baidugeocoding;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Created by yinyankun on 2017/6/8.
 * 数据存储
 */
public class AddressData {
    public static boolean appendDate(File csvFile, List<String> data){
        try {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile, true), "UTF-8"), 1024);
            for(int i=0; i<data.size(); i++){
                String tempData = data.get(i);
                StringBuffer sb = new StringBuffer();
                sb.append(tempData+"\n");
                bw.write(sb.toString());
                if(i%1000==0)
                    bw.flush();
            }
            bw.flush();
            bw.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
