package yunkeyyk.baidugeocoding;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yinyankun on 2017/6/8.
 * 读取本地数据
 */
public class OriData {

    /**
     * 读取文件的指定行数据
     * params:Number 指定哪一行
     * */

    public static String getSpecialData(String filePath,Integer Number) throws IOException {
        FileReader fileReader = new FileReader(new File(filePath));
        LineNumberReader lineNumberReader = new LineNumberReader(fileReader);
        String line="" ;
        String oneLine="";
        Integer num=-1;
        while ((line=lineNumberReader.readLine())!=null){
            //System.out.println(lineNumberReader.getLineNumber()+":"+line);
            if(lineNumberReader.getLineNumber()!=Number){
               continue;
            }else{
                oneLine=line;
                num=Integer.valueOf(lineNumberReader.getLineNumber());
                break;
            }
        }
      return num+":"+oneLine;
    }


    /**
     * txt文件共有多少条数据
     * */
    public static Integer getTotalLine(String filePath)throws IOException{
        FileReader fileReader=new FileReader(new File(filePath));
        LineNumberReader lineNumberReader=new LineNumberReader(fileReader);
        String s=lineNumberReader.readLine();
        Integer line=0;
        while (s!=null){
            line++;
            //s=lineNumberReader.readLine();
//            if(line>=2){
//                if(s!=null){
//                    System.out.print(s);
//                }
//            }
        }
        lineNumberReader.close();
        fileReader.close();
        System.out.println("***:"+line);
        return line;
    }


    public static List<String> getData(String filePath,Integer beginNumber,Integer step){

        File file=new File(filePath);
        BufferedReader bufferedReader=null;
        try{
            bufferedReader=new BufferedReader(new FileReader(file));
        }catch (FileNotFoundException f){
            f.printStackTrace();
        }

        String line="";
        String oneLine="";
        List<String> allLines=new ArrayList<String>();

        try{
            while ((line=bufferedReader.readLine())!=null){
                oneLine=line;
                allLines.add(oneLine);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return allLines;
    }
}
