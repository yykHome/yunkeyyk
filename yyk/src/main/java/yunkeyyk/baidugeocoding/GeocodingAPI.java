package yunkeyyk.baidugeocoding;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.text.DecimalFormat;

/**
 * Created by yinyankun on 2017/6/8.
 * 实现功能：根据经纬度，调用百度地图接口，解析返回的json数据，
 * 得到对应地址信息（country，province，city，district，street）
 */
public class GeocodingAPI {
    public static final String filePath="D:\\address_data\\crm_t_clue.txt";
    public static final String savePath="D:\\address_data\\process\\adress.csv";

    public static void main(String[] args) throws Exception {

        //每次批量读取后，存储
        List<String> allAddress=new ArrayList<String>();

        DecimalFormat df=new DecimalFormat("0.000000");
        /**
         * 经纬度本地获取，数量：19，000，000
        * */
        int startLine=300001;
        int endLine=100000;
        for(int i=31;i<=30000;i++){

            Thread.sleep(100);
            String oriInfo=OriData.getSpecialData(filePath,i);
            //System.out.println(oriInfo);
            //System.out.println("");
            String ss=oriInfo.replace("\"","");
            String[] s=ss.split("\t");
           // System.out.print(df.format(Double.valueOf(s[2]))+":"+df.format(Double.valueOf(s[3])));
            String lat=df.format(Double.valueOf(s[3]));
            String lon=df.format(Double.valueOf(s[2]));

            if( (71<Double.valueOf(lon)&& Double.valueOf(lon)<137)
                    && 3<Double.valueOf(lat)&& Double.valueOf(lat)<54){//大致判断经纬度在中国
                String location=lat+","+lon;
                String address=WebServiceGeocoding.getAdress(location);
                String addr=s[0]+","+s[1]+","+address;
                System.out.println(addr);
                allAddress.add(addr);
            }else{
                String addr=s[0]+","+s[1]+","+"非国内数据";
                System.out.println(s[0]+","+s[1]+","+addr);
                allAddress.add(addr);
            }
        }

        //System.out.print(allAddress.size());
        //System.out.print(allAddress.get(0));
        File outFile = new File(savePath);//写出的CSV文件
        AddressData.appendDate(outFile,allAddress);
    }
}
