package yunkeyyk.baidugeocoding;

import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by yinyankun on 2017/6/8.
 * 发送url请求，返回地址信息
 */
public class WebServiceGeocoding {
    public static final String key="HvGGaSYjMcGeBvw841x5LqBuvAFZ0PDE";

    public static String getAdress(String location){
        String addressInfos="";
        String url="http://api.map.baidu.com/geocoder/v2/?"+
                "location="+ location
                +"&output=json&ak="+key;
        //url=http://api.map.baidu.com/geocoder/v2/?callback=renderReverse&location=39.983424,116.322987&output=json&pois=1&ak=您的ak
        JSONObject jsonobject = JSONObject.fromObject(loadJson(url));
        //System.out.println(jsonobject.toString());

        /**
         * 解析json数据，得到其中包含的地址信息
         * */
        JSONObject resultsArray = jsonobject.getJSONObject("result");
        //System.out.print(resultsArray);
        JSONObject infoObject = resultsArray.getJSONObject("addressComponent");
        String contry = infoObject.getString("country");
        String province=infoObject.getString("province");
        String city=infoObject.getString("city");
        String district=infoObject.getString("district");
        // String adcode=infoObject.getString("adcode");
        String street=infoObject.getString("street");
        String street_number=infoObject.getString("street_number");
        addressInfos=contry+","+province+","+city+","+district+","+street+""+street_number;
        return addressInfos;
    }

    /**
     * 发送 http 请求
     * */
    public static String loadJson (String url) {
        StringBuilder json = new StringBuilder();
        try {
            //下面那条URL请求返回结果无中文，可不转换编码格式
            URL urlObject = new URL(url);
            URLConnection uc = urlObject.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
}
