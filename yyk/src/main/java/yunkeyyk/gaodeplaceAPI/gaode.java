package yunkeyyk.gaodeplaceAPI;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * gaode PlaceAPI ,随机给出经纬度，抓取附近的poi
* */
public class gaode {
    final static String saveFile="D:\\gaodeInfo.csv";
    final static String[] keys=new String[]{"dd664c44f86f1f0b8355b377d5899f5f","90b1575ae74dbe9a24d07f7a730c04c8",
                                             "fd6329856b21b17154b1839e51c12529","79d9804060d5aa9457e139ab0efcbcd3"};
    final static int[] count=new int[4];

    /*
     * Java http 请求
     */
    public static String loadJson (String url) {
        StringBuilder json = new StringBuilder();
        try {
            //下面那条URL请求返回结果无中文，可不转换编码格式
            URL urlObject = new URL(url);
            URLConnection uc = urlObject.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static Integer getpoiCount(String location) {
        System.out.println("&&&");

        Random r= new Random();
        int index=r.nextInt(4);
        String key=keys[index];
        count[index]++;
        String keywords = getKeywords();
        String type = getType();

        String searchurl = "http://restapi.amap.com/v3/place/around?"
                   + "key=" + key
                   + "&location=" + location
                   + "&keywords=" + keywords
                   + "&types=" + type
                   + "&radius=" + "50000"
                   + "&offset=25"
                   + "&page=" + "1"
                   + "&extensions=all";
        JSONObject jsonobject = JSONObject.fromObject(loadJson(searchurl));
       // System.out.println(jsonobject.toString());
        //解析json数据
        String searchCount = jsonobject.getString("count");

        //System.out.print(searchCount);
        Integer count=Integer.valueOf(searchCount);
        return count;
    }

    public static void getpoiInfo(String location,String page) {
        System.out.println("***");

        Random r= new Random();
        int index=r.nextInt(4);
        String key=keys[index];
        String keywords=getKeywords();
        String type=getType();

        String searchurl="http://restapi.amap.com/v3/place/around?"
                +"key="+key
                +"&location="+location
                +"&keywords="+keywords
                +"&types="+type
                +"&radius="+"50000"
                +"&offset=25"
                +"&page="+page
                +"&extensions=all";

        JSONObject jsonobject = JSONObject.fromObject(loadJson(searchurl));
        System.out.println(jsonobject.toString());
        List<String >  poiInfo=new ArrayList<String>();
        poiInfo.add(jsonobject.toString());

//        //解析json数据
//        JSONArray resultsArray = jsonobject.getJSONArray("pois");
//        List<String >  poiInfo=new ArrayList<>();
//        String searchCount=jsonobject.getString("count");
//        //System.out.print(searchCount);
//        Integer count=Integer.valueOf(searchCount);
//        if(count>0){
//            for(int i=0;i<25;i++){
//                String infos="";
//                JSONObject infoObject = resultsArray.getJSONObject(i);
//                String idInfo = infoObject.getString("id");
//                String nameInfo = infoObject.getString("name");
//                String tagInfo = infoObject.getString("tag");
//                String typeInfo = infoObject.getString("type");
//                String typecodeInfo = infoObject.getString("typecode");
//                String addressInfo = infoObject.getString("address");
//                String locationInfo = infoObject.getString("location");
//                String telInfo = infoObject.getString("tel");
//                String pnameInfo = infoObject.getString("pname");
//
//                infos=idInfo+","+nameInfo+","+tagInfo+","+typeInfo+","+typecodeInfo+","+addressInfo+","+locationInfo+","+telInfo+","+pnameInfo+"\n";
//                poiInfo.add(infos);
//
//            }
//        }else{
//            System.out.print("查询为空！");
//        }

        File outFile = new File(saveFile);//写出的CSV文件
        appendDate(outFile,poiInfo);

    }

    public static boolean appendDate(File csvFile, List<String> data){
        try {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile, true), "UTF-8"), 1024);
            for(int i=0; i<data.size(); i++){
                String tempData = data.get(i);
                StringBuffer sb = new StringBuffer();
                sb.append(tempData+"\n");
                bw.write(sb.toString());
                if(i%1000==0)
                    bw.flush();
            }

            bw.flush();
            bw.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void getLocation(){
        Double minLat=38.000000;//纬度
        Double maxLat=42.000000;//
        Double minLon=115.000000;//经度
        Double maxLon=117.000000;
        double diffLat=4.300000-3.860000;
        double diffLon=74.110000-73.660000;


        for(double startLat=minLat+diffLat;startLat<maxLat;){
            for(double startLon=minLon+diffLon;startLon<maxLon;){

                String location=startLon+","+startLat;

                //String location="116.581488,40.590464";
                Integer count=getpoiCount(location);
                if(count>0){
                    for(int i=0;i<Integer.valueOf(count/25);i++){
                        getpoiInfo(location,String.valueOf(i+1));
                    }
                }else {
                    continue;
                }
                startLon+=2*diffLon;
            }
            startLat+=2*diffLat;
        }

    }
    public static String getKeywords(){
        return "餐饮相关场所";
    }
    public static String getType(){
        return "050000";
    }

    public static void main(String[] args) {
        //注意：高德最多取小数点后六位
        getLocation();
    }
}