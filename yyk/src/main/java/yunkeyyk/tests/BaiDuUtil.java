package yunkeyyk.tests;

import net.sf.json.JSONObject;

/**
 * 百度工具
 */
public class BaiDuUtil {
    public static String getCity(String lat, String lng) {
        JSONObject obj = getLocationInfo(lat, lng).getJSONObject("result");
        return obj.getString("name");
    }

    public static JSONObject getLocationInfo(String lat, String lng) {
        String url = "http://api.map.baidu.com/place/v2/search?query=酒店&page_size=10&page_num=0&radius=20000&location=" + lat + ","
                + lng + "&output=json&ak=" +"zfNUpVzcm6v6jIiUewOa8fC5sbFiaMG3"+"&pois=1";
        String urll="http://api.map.baidu.com/place/v2/search?query=酒店&page_size=10&page_num=0&scope=1&location=39.915,116.404&radius=2000&output=json&ak=zfNUpVzcm6v6jIiUewOa8fC5sbFiaMG3";
        String url2="http://api.map.baidu.com/place/v2/search?query=银行&bounds=39.915,116.404,39.975,116.414&output=json&ak=zfNUpVzcm6v6jIiUewOa8fC5sbFiaMG3";
        JSONObject obj = JSONObject.fromObject(HttpUtil.getRequest(urll));

        return obj;
    }

    public static void main(String[] args) {
        //39.915,116.404  38.76623,116.43213
        System.out.println(BaiDuUtil.getLocationInfo("38.7662300000000", "116.4321300000000"));
    }
}
