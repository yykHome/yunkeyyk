package yunkeyyk.tests;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.sql.*;

//http://my.oschina.net/Thinkeryjgfn/blog/177283
//http://www.cnblogs.com/I-will-be-different/p/3925351.html?utm_source=tuicool&utm_medium=referral
//java jdbc使用SSH隧道连接mysql数据库demo
public class sqlconn {
    public static void go() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("root", "123.56.203.191", 22);
            session.setPassword("yunkeML100022+&^33");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            System.out.println(session.getServerVersion());//这里打印SSH服务器版本信息

            //ssh -L 192.168.0.102:5555:192.168.0.101:3306 yunshouhu@192.168.0.102  正向代理
            int assinged_port = session.setPortForwardingL("123.56.203.191",3302, "rr-2zeg40364h2thw9m6.mysql.rds.aliyuncs.com", 3306);//端口映射 转发

            System.out.println("localhost:" + assinged_port);

            //ssh -R 192.168.0.102:5555:192.168.0.101:3306 yunshouhu@192.168.0.102
            //session.setPortForwardingR("192.168.0.102",5555, "192.168.0.101", 3306);
            // System.out.println("localhost:  -> ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try{
            System.out.println("=============");
            go();
            Connection conn = null;
            conn = DriverManager.getConnection("jdbc:mysql://123.56.203.191:5555/mysql", "yunker", "yunker2016EP");
            //getData(conn);


        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        // URL指向要访问的数据库名world
        String url = "jdbc:mysql://rds0710650me01y6d3ogo.mysql.rds.aliyuncs.com/yunketest";
        // MySQL配置时的用户名
        String user = "yunker";
        // MySQL配置时的密码
        String password = "yunke2016";

        try {
            // 加载驱动程序
            Class.forName(driver);
            // 连续数据库
            Connection conn = DriverManager.getConnection(url, user, password);
            if(!conn.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            // statement用来执行SQL语句
            Statement statement = conn.createStatement();
            // 要执行的SQL语句
            String sql = "select * from crm_t_clue limit 1;";
            // 结果集
            ResultSet rs = statement.executeQuery(sql);
            System.out.println(rs.absolute(0));

            rs.close();
            conn.close();
        }
        catch(ClassNotFoundException e) {
            System.out.println("Sorry,can`t find the Driver!");
            e.printStackTrace();
        } catch(SQLException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}