package yunkeyyk.tests;

/**
 * Created by Administrator on 2017/6/1.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

import static sun.misc.Version.print;

/**
 * java根据url获取json对象
 * @author openks
 * @since 2013-7-16
 * 需要添加java-json.jar才能运行
 */
public class baidutest {
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            System.out.print(jsonText);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
            // System.out.println("同时 从这里也能看出 即便return了，仍然会执行finally的！");
        }
    }
    public static String loadJSON (String url) {
        StringBuilder json = new StringBuilder();
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        System.out.print(json.length());
        System.out.print(json.toString());
        return json.toString();

    }
    public static void main(String[] args) throws IOException, JSONException {
        //http://api.map.baidu.com/place/v2/search?query=酒店&page_size=10&page_num=0&scope=1&location=39.915,116.404&radius=2000&output=json&ak={您的密钥}
       //多关键词并集检索
       // http://api.map.baidu.com/place/v2/search?query=酒店$银行&scope=2&output=json&location=39.915168125773725,116.40998741769683&radius=2000&filter=sort_name:distance|sort_rule:1&ak={您的密钥}
        String url="http://api.map.baidu.com/place/v2/search?query=派出所&page_size=10&page_num=0&scope=2&location=39.915,116.404&radius=200000&output=json&ak=zfNUpVzcm6v6jIiUewOa8fC5sbFiaMG3";
        String json = loadJSON(url);


    }
}