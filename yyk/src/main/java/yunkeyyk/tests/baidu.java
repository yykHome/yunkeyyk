package yunkeyyk.tests;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/5.
 */
public class baidu {


    /*
    * Java http 请求
    */
    public static String loadJson (String url) {
        StringBuilder json = new StringBuilder();
        try {
            //下面那条URL请求返回结果无中文，可不转换编码格式
            URL urlObject = new URL(url);
            URLConnection uc = urlObject.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static List<String> getdata(){
        //读取本地数据
        File csv = new File("D:\\address_data\\test.csv");  // CSV文件路径
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new FileReader(csv));
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        List<String> allString = new ArrayList();
        try {

            while ((line = br.readLine()) != null)  //读取到的内容给line变量
            {
                everyLine = line;
               // System.out.println(everyLine);
                allString.add(everyLine);
            }
           // System.out.println("all lines:"+allString.size());
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return allString;

    }
    public static String getAdress(String location){
        String addressInfos="";
        String key="liUHW3MNPW1gmKypjhGe0rfTcDo8eT2G";
        //HvGGaSYjMcGeBvw841x5LqBuvAFZ0PDE
        String url="http://api.map.baidu.com/geocoder/v2/?"+
                "location="+ location
                +"&output=json&ak="+key;
        JSONObject jsonobject = JSONObject.fromObject(loadJson(url));
        //System.out.println(jsonobject.toString());

        JSONObject resultsArray = jsonobject.getJSONObject("result");
        JSONObject infoObject = resultsArray.getJSONObject("addressComponent");
        String contry = infoObject.getString("country");
        String province=infoObject.getString("province");
        String city=infoObject.getString("city");
        String district=infoObject.getString("district");
       // String adcode=infoObject.getString("adcode");
        String street=infoObject.getString("street");
        String street_number=infoObject.getString("street_number");
        addressInfos=contry+","+province+","+city+","+district+","+street+""+street_number;
        return addressInfos;
    }

    public static boolean appendDate(File csvFile, List<String> data){
        try {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile, true), "UTF-8"), 1024);
            for(int i=0; i<data.size(); i++){
                String tempData = data.get(i);
                StringBuffer sb = new StringBuffer();
                sb.append(tempData+"\n");
                bw.write(sb.toString());
                if(i%1000==0)
                    bw.flush();
            }
            bw.flush();
            bw.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void main(String[] args) {
        List<String> allAddress=getdata();

        List<String> addressAll=new ArrayList<String>();
        for(int i=0;i<allAddress.size();i++){
            String[] s=allAddress.get(i).split(",");
            if( (71<Double.valueOf(s[2])&& Double.valueOf(s[2])<137)
                    && 3<Double.valueOf(s[3])&& Double.valueOf(s[3])<54){//大致判断经纬度在中国
                String location=s[3]+","+s[2];
                String address=getAdress(location);
                String addr=s[0]+","+s[1]+","+address;
                System.out.println(s[0]+","+address);
                addressAll.add(addr);
            }
        }
        System.out.print(addressAll.size());
        String saveFile="D:\\address_data\\process\\adress.csv";
        File outFile = new File(saveFile);//写出的CSV文件
        appendDate(outFile,addressAll);

    }
}
